import React from 'react';
import './App.css';
import logo from './logo.webp';
import Login from './components/Login/Login';
import useToken from './components/App/useToken';
import Character from './components/Character/Character';
import Fight from './components/Fight/Fight';
import NewFight from './components/Fight/NewFight';
import History from './components/History/History';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

function App() {
  const { token, setToken, removeToken } = useToken();

  if (!token) {
    return <Login setToken={setToken} />
  }

  return (
    <div className="App">
      <Router>
        <header className="App-header col-md-12">
          <Link to="/"><img src={logo} alt="Logo" className="logo" /></Link>
          <p>A small RPG using an API in React</p>
          <button
            type="submit"
            className="btn btn-primary btn-logout"
            onClick={removeToken}>Log out</button>
        </header>
        <main className="App-body col-md-12">
          <Switch>
            <Route exact path="/"><Character token={token} /></Route>
            <Route path="/newfight/:id"><NewFight token={token} /></Route>
            <Route path="/history/:id"><History /></Route>
            <Route path="/fight/:id"><Fight token={token} /></Route>
          </Switch>
        </main>
      </Router>
    </div>
  );
}

export default App;
