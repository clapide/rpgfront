import React from 'react';

class cardCharacter extends React.Component {
  render() {
    const character = this.props.character;
    const opponent = this.props.opponent;
    const fight = this.props.fight;
    const logs = fight.logs;
    const winner = fight.winner;
    const round = fight.round;

    return (
      <>
      <div className="card" style={{width: "18rem"}} >
        <div className="card-header">
          <h5 className="card-title">You</h5>
        </div>
        <div className="card-body">
          <p className="card-text">Rank - {character.rank}</p>
          <p className="card-text">Health - {character.health}</p>
          <p className="card-text">Attack - {character.attack}</p>
          <p className="card-text">Defense - {character.defense}</p>
          <p className="card-text">Magik - {character.magik}</p>
          { !winner && round % 2 === 0 &&
            <span className="actionCharacter">
              <button 
                className="btn btn-primary"
                onClick={() => this.props.onAttack()}>Attack</button>
            </span>
          }
        </div>
        <img src="https://parlersport.com/wp-content/uploads/2020/09/1600715927_Apex-Legends-Buff-de-la-saison-6-a-fait-de.jpg" alt="character" className="card-img-bottom" />
      </div>

      <div className="card" style={{width: "30rem"}} >
        <div className="card-header">
          <h5 className="card-title">Logs</h5>
        </div>
        <ul className="list-group list-group-flush" >
            { logs.map((log, index) => (
              <li key={index} className="list-group-item">
                <p className="card-text">Round - {index}: {log}</p>
              </li>
            ))}
          </ul>
      </div>

      <div className="card" style={{width: "18rem"}} >
        <div className="card-header">
          <h5 className="card-title">Enemy :c</h5>
        </div>
        <div className="card-body">
          <p className="card-text">Rank - {opponent.rank}</p>
          <p className="card-text">Health - {opponent.health}</p>
          <p className="card-text">Attack - {opponent.attack}</p>
          <p className="card-text">Defense - {opponent.defense}</p>
          <p className="card-text">Magik - {opponent.magik}</p>
          { !winner && round % 2 !== 0 &&
            <span className="actionCharacter">
              <button 
                className="btn btn-primary"
                onClick={() => this.props.onAttack()}>Enemy's tour</button>
            </span>
          }
        </div>
        <img src="https://www.teahub.io/photos/full/269-2699701_apex-legends-wraith-kunai-heirloom-4k-wraith-apex.jpg" alt="character" className="card-img-bottom" />
      </div>
      </>
    );
  }
}

export default cardCharacter;
