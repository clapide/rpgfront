import React from 'react';
import { withRouter, Link } from "react-router-dom";
import Card from './cardCharacter';

class Fight extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      fight: {},
      initiator: {},
      opponent: {}
    }
  }

  handleAttack() {
    const id = this.props.match.params.id;
    fetch(`http://localhost:3000/fights/attack/${id}`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'authorization': this.props.token
      }
    })
    .then(res => res.json())
    .then((updatedFight) => {
      const initiator = Object.assign(this.state.initiator);
      initiator.health = updatedFight.initiatorHP;
      const opponent = Object.assign(this.state.opponent);
      opponent.health = updatedFight.opponentHP;
      this.setState({fight: updatedFight, initiator, opponent});
    });
  }

  componentDidMount() {
    const id = this.props.match.params.id;
    fetch(`http://localhost:3000/fights/details/${id}`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'authorization': this.props.token
      }
    })
    .then(res => res.json())
    .then((newFight) => {
      const { fight, initiator, opponent } = newFight;
      this.setState({fight, initiator, opponent});
    });
  }

  render(){
    const initiator = this.state.initiator;
    const fight = this.state.fight;
    const opponent = this.state.opponent;
    return(
      <>
        <h1>FIGHT!!!</h1>
        <div className="row">
          { initiator && initiator.id &&
            <Card
              character={initiator}
              opponent={opponent}
              fight={fight}
              onAttack={this.handleAttack.bind(this)}
            />
          }
        </div>
        { fight.winner &&
          <Link to="/">
            <button className="btn btn-primary">Home</button>
          </Link>
        }
      </>
    )
  }
}

export default withRouter(Fight);