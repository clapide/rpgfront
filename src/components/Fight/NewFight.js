import React from 'react';
import { Redirect, withRouter, Link } from "react-router-dom";

class Fight extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      fightId: -1,
      error: ""
    }
  }

  componentDidMount() {
    const id = this.props.match.params.id;
    fetch(`http://localhost:3000/fights/create/`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'authorization': this.props.token
      },
      body: JSON.stringify({ initiatorId: id })
    })
    .then(res => res.json())
    .then((newFight) => {
      if (newFight.error) {
        this.setState({error: newFight.error});
      } else {
        this.setState({fightId: newFight.fight.id});
      }
    });
  }

  render(){
    let html = '';
    if (this.state.fightId >= 0) {
      html = <Redirect to={`/fight/${this.state.fightId}`} />
    } else if (this.state.error !== "") {
      html = <NoFight />
    }
    return(
      <>
        {html}
      </>
    )
  }
}

function NoFight() {
  return(
    <>
      <h1>No opponent find, sorry.</h1>
      <div className="row">
      <Link to="/">
        <button className="btn btn-primary">Home</button>
      </Link>
      </div>
    </>
  );
}

export default withRouter(Fight);