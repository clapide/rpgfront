import React from 'react';
import { Link } from "react-router-dom";

class DetailsCharacter extends React.Component {
  render() {
    const character = this.props.character;
    const initialCharacter = this.props.initialCharacter;
    const update = this.props.character.skillpoint > 0;
    const oneHourAgo = new Date(new Date().setHours(new Date().getHours() - 1));

    return (
      <div className="card" style={{width: "18rem"}} >
        <div className="card-body">
          <span>
            <h5 className="card-title">Rank - {character.rank}</h5>
            <button
              onClick={() => this.props.onDelete(character.id)}
              className="btn btn-danger btn-sm deleteCharacter">X</button>
          </span>
          <p className="card-text">
          { initialCharacter.health < character.health &&
            <button className="btn btn-outline-secondary btn-sm"
            onClick={() => this.props.onClick(
              character.id,
              {health: character.health - 1},
              true
            )}>-</button>
          }Health - {character.health}
          { update &&
            <button className="btn btn-outline-secondary btn-sm"
            onClick={() => this.props.onClick(
              character.id,
              {health: character.health + 1},
              false
            )}>+</button>
          }
          </p>
          <p className="card-text">
            { initialCharacter.attack < character.attack &&
              <button className="btn btn-outline-secondary btn-sm"
              onClick={() => this.props.onClick(
                character.id,
                {attack: character.attack - 1},
                true
              )}>-</button>
            }
            Attack - {character.attack}
          { update &&
            <button className="btn btn-outline-secondary btn-sm"
            onClick={() => this.props.onClick(
              character.id,
              {attack: character.attack + 1},
              false
            )}>+</button>
          }
          </p>
          <p className="card-text">
            { initialCharacter.defense < character.defense &&
              <button className="btn btn-outline-secondary btn-sm"
              onClick={() => this.props.onClick(
                character.id,
                {defense: character.defense - 1},
                true
              )}>-</button>
            }
            Defense - {character.defense}
            { update &&
              <button className="btn btn-outline-secondary btn-sm"
              onClick={() => this.props.onClick(
                character.id,
                {defense: character.defense + 1},
                false
              )}>+</button>
            }
          </p>
          <p className="card-text">
            { initialCharacter.magik < character.magik &&
              <button className="btn btn-outline-secondary btn-sm"
              onClick={() => this.props.onClick(
                character.id,
                {magik: character.magik - 1},
                true
              )}>-</button>
            }
            Magik - {character.magik}
            { update &&
              <button className="btn btn-outline-secondary btn-sm"
              onClick={() => this.props.onClick(
                character.id,
                {magik: character.magik + 1},
                false
              )}>+</button>
            }
          </p>
          <p className="card-text">Skill points - {character.skillpoint}</p>
          <span className="actionCharacter">
            { (!character.lastLoss || character.lastLoss > oneHourAgo) &&
              <Link to={"/newfight/" + character.id}>
                <button className="btn btn-primary">Fight</button>
              </Link>
            }
            <Link to={"/history/" + character.id}>
              <button className="btn btn-secondary">History</button>
            </Link>
            { JSON.stringify(initialCharacter) !== JSON.stringify(character)  &&
              <>
                <button className="btn btn-success"
                onClick={() => this.props.onSubmit(character)}>update</button>
                <button className="btn btn-warning"
                onClick={() => this.props.onCancel(character.id)}>cancel</button>
              </>
            }
          </span>
        </div>
        <img src="https://parlersport.com/wp-content/uploads/2020/09/1600715927_Apex-Legends-Buff-de-la-saison-6-a-fait-de.jpg" alt="character" className="card-img-bottom" />
      </div>
    );
  }
}

export default DetailsCharacter;
