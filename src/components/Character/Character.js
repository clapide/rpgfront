import React from 'react';
import Details from './detailsCharacter'

class Character extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      history: []
    }
  }

  // create new Character
  handleCreate() {
    fetch(`http://localhost:3000/characters/create`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'authorization': this.props.token
      }
    })
    .then(res => res.json())
    .then((character) => {
      const history = this.state.history;
      const initialCharacter = history[0].characters.slice();
      const current = history[history.length - 1];
      let characters = current.characters.slice();
      characters = characters.concat(character);
      history[0] = {characters: initialCharacter.concat(character)};

      this.setState({
        history: history.concat([{ characters }])
      });
    });
  }

  // update character stats
  handleClick(characterId, json, isMinus) {
    const history = this.state.history;
    const current = history[history.length - 1];
    const characters = current.characters.slice();

    // not really proud of it
    // got trouble to copy with immutability
    const index = characters.map(x => x.id).indexOf(characterId)
    let character = { ...characters[index] };
    if (character.id === characterId) {
      if (isMinus) {
        character.skillpoint++;
      } else {
        character.skillpoint--;
      }
      character = Object.assign(character, json);

      characters[index] = character;
      this.setState({
        history: history.concat([{ characters }])
      });
    }
  }

  handleCancel(characterId) {
    const history = this.state.history;
    const initialCharacter = history[0].characters.slice();
    const characters = history[history.length - 1].characters.slice();
    // not really proud of it
    // got trouble to copy with immutability
    const index = characters.map(x => x.id).indexOf(characterId)
    let character = { ...initialCharacter[index] };
    if (character.id === characterId) {
      characters[index] = character;
      this.setState({
        history: history.concat([{ characters }])
      });
    }
  }

  // update character stats
  handleUpdate(character) {
    fetch(`http://localhost:3000/characters/lvlUp`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'authorization': this.props.token
      },
      body: JSON.stringify(character)
    })
    .then(res => res.json())
    .then((character) => {
      const history = this.state.history;
      const current = history[history.length - 1];
      let characters = current.characters.slice();
      // not really proud of it
      // got trouble to copy with immutability
      const index = characters.map(x => x.id).indexOf(character.id)
      characters[index] = character;
      history[0] = {characters};

      this.setState({
        history: history.concat([{ characters }])
      });
    });
  }

  // delete character
  handleDelete(characterId) {
    fetch(`http://localhost:3000/characters/delete`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'authorization': this.props.token
      },
      body: JSON.stringify({id: characterId})
    })
    .then((res) => {
      if (res.status === 201) {
        const history = this.state.history;
        const current = history[history.length - 1];
        let characters = current.characters.slice();
        // not really proud of it
        // got trouble to copy with immutability
        characters = characters.filter(x => x.id !== characterId)

        this.setState({
          history: history.concat([{ characters }])
        });
      }
    });
  }

  componentDidMount() {
    fetch(`http://localhost:3000/characters/list`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'authorization': this.props.token
      }
    })
    .then(res => res.json())
    .then((characters) => {
      this.setState({
        history: this.state.history.concat([{
          characters: characters,
        }]),
      })
    });
  }

  render() {
    const history = this.state.history;
    if (history.length <= 0) {
      return(<></>);
    }

    const current = history[history.length - 1].characters;
    const initial = history[0].characters;

    return(
      <>
        <div className="row">
          {current.map((character, index) => (
            <Details
              key={character.id}
              character={character}
              initialCharacter={initial[index]}
              onClick={this.handleClick.bind(this)}
              onCancel={this.handleCancel.bind(this)}
              onSubmit={this.handleUpdate.bind(this)}
              onDelete={this.handleDelete.bind(this)}
            />
          ))}
        </div>
        <button
          type="submit"
          className="btn btn-primary"
          onClick={() => this.handleCreate()}>
          Create new character
        </button>
      </>
    )
  }
}

export default Character;