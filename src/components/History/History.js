import React from 'react';
import { withRouter, Link } from 'react-router-dom';
import CardHistory from './CardHistory';

class Character extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      history: []
    }
  }

  componentDidMount() {
    const id = this.props.match.params.id;
    fetch(`http://localhost:3000/fights/list/${id}`, {
      method: 'GET',
      headers: {'Content-Type': 'application/json'}
    })
    .then(res => res.json())
    .then(fights => this.setState({ history: fights }));
  }

  render() {
    const history = this.state.history;
    if (history.length <= 0) {
      return(<></>);
    }
    const id = this.props.match.params.id;

    return(
      <>
        <div className="row">
          {history.map((fight, index) => (
            <span key={index}><CardHistory fight={fight} you={id}/></span>
          ))}
        </div>
        <Link to="/">
          <button className="btn btn-primary">Home</button>
        </Link>
      </>
    )
  }
}

export default withRouter(Character);