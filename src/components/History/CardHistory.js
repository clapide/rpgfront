import React from 'react';

class CardHistory extends React.Component {
  render() {
    const fight = this.props.fight;
    const yourId = parseInt(this.props.you);

    console.log(fight);

    let winner = 'Fight not finished';
    if (fight.winner === yourId) {
      winner = 'Winner: You!';
    } else if (fight.winner) {
      winner = 'Winner: Ennemy';
    }

    let yourHealth;
    let ennemyHealth;
    if (fight.initiator === yourId) {
      yourHealth = fight.initiatorHP;
      ennemyHealth = fight.opponentHP;
    } else {
      ennemyHealth = fight.initiatorHP;
      yourHealth = fight.opponentHP;
    }
    return (
      <div className="card" style={{width: "18rem"}} >
        <div className="card-header">
          <h5 className="card-title">{winner}</h5>
        </div>
        <div className="card-body">
          <p className="card-text">Your health - {yourHealth}</p>
          <p className="card-text">Ennemy health - {ennemyHealth}</p>
        </div>
        <ul className="list-group list-group-flush" >
          { fight.logs.map((log, index) => (
            <li key={index} className="list-group-item">
              <p className="card-text"><b>Round #{index}:</b> {log}</p>
            </li>
          ))}
        </ul>
      </div>
    );
  }
}

export default CardHistory;
