import React, { useState } from 'react';
import PropTypes from 'prop-types';

async function loginUser(credentials) {
  return fetch('http://localhost:3000/users/login', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(credentials)
  }).then(data => data.json())
}

export default function Login({ setToken }) {
  const [email, setEmail] = useState();
  const [userName, setUserName] = useState();
  const [password, setPassword] = useState();

  const handleSubmit = async e => {
    e.preventDefault();
    const token = await loginUser({email, userName, password});
    setToken(token);
  }

  return(
    <main className="App-body col-md-12">
      <form onSubmit={handleSubmit}>
        <div className="form-group">
          <label htmlFor="inputEmail1">Email address</label>
          <input 
            type="email"
            className="form-control"
            aria-describedby="emailHelp"
            placeholder="Enter email"
            onChange={e => setEmail(e.target.value)} />
        </div>
        <div className="form-group">
          <label htmlFor="inputEmail1">User Name</label>
          <input 
            type="userName"
            className="form-control"
            placeholder="Enter user name"
            onChange={e => setUserName(e.target.value)} />
        </div>
        <div className="form-group">
          <label htmlFor="inputPassword1">Password</label>
          <input
            type="password"
            className="form-control"
            placeholder="Password"
            onChange={e => setPassword(e.target.value)} />
        </div>
        <button type="submit" className="btn btn-primary">Submit</button>
      </form>
    </main>
  )
}

Login.propTypes = {
    setToken: PropTypes.func.isRequired
};
