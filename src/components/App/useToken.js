import { useState } from 'react';
import jwt_decode from 'jwt-decode';

export default function useToken() {
    const getToken = () => {
      const tokenStringify = localStorage.getItem('token');
      const token = JSON.parse(tokenStringify)?.token;
      if (token) {
        const { exp } = jwt_decode(token);
        const expirationTime = (exp * 1000);
        if (Date.now() >= expirationTime) {
            localStorage.removeItem('token')
            setToken();
        }
      }
      return token;
    };

    const saveToken = (token) => {
      localStorage.setItem('token', JSON.stringify(token));
      setToken(token.token);
    };

    const removeToken = () => {
      localStorage.removeItem('token');
      setToken();
    };

    const [token, setToken] = useState(getToken());

    return {
        setToken: saveToken,
        removeToken: removeToken,
        token
    }
}